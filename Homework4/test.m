clear
clc

[R,T]=meshgrid(1:.1:2,pi/4:pi/16:pi);
X=R.*cos(T);Y=R.*sin(T);
Z=X.^2-2*Y.^2;
pcolor(X,Y,Z);
axis equal;
shading interp;

%f = 

%I = integral(Z,-1,1)