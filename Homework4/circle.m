clear
clc
%Circle

%Center coordinates of Circle
xc = 0; yc = 0;
%Radius of circle
r = 6;

angle = 0:0.01:2*pi;
xp = r*cos(angle);
yp = r*sin(angle);

hold all
plot(xc+xp,yc+yp,'LineWidth',2)
axis([-7 7 -7 7])
grid on
xlabel('x')
ylabel('y')

xx = 0:0.01:2.2; yy = 1.55455*xx;
plot(xx,yy,'LineWidth',3)
x = 2.2; y = 3.42;
plot(x,y,'k*','MarkerSize',12)
legend('','r','(x,y) = (2.2,3.42)')
