clear
clc

% Cartesian Coordinates (x,y)
%x = 0:0.01:2.16; y = 1.56019*x;

%hold all
%plot(x,y,'r-','MarkerSize',5)
%plot(2.16,3.37,'b*','MarkerSize',7)
%legend('r = 4','(x,y) = (2.16,3.37)')

grid on
axis([0 8 0 8])
xlabel('r')
ylabel('theta')

x1 = 0;
x2 = 6;
y1 = 0;
y2 = 6;

xx = [x1, x2, x2, x1, x1];
yy = [y1, y1, y2, y2, y1];
hold all
plot(xx,yy,'b-','LineWidth',3);
% Polar Coordinates (r,theta)
r = 4.07;
theta = 1;
plot(r,theta,'r*','MarkerSize',10);

legend('square','(r,theta) = (4.07,1)')


