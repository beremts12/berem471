
+++++++++++++++++++++++++++++++++++++++++++
README file for Berenise Miramontes MATH471
+++++++++++++++++++++++++++++++++++++++++++


1. HOMEWORK 1:



        - The report for Homemwork 1 is in the folder labeled Homework1
          and is called 'Homework1report.txt.' This report explains how to
          compile and run my java code for this assignment.

        - The Hello World code for this homeowork is also located in the
          folder Homework1, but is in a separate folder labeled 'Codes.'
             - Arely & Berenise Miramontes


2. Homework 2:

   	- Homework 2 can be found under the folder Homework2 under my
	  repository. 

	- Using Newton’s method to find the root of a function is one of 
	  many methods to do so. In the Newton’s Method we start by stating 
	  an initial guess, x0. Next, we draw the tangent line of the 
	  function y = f(x). Using this tangent line we intersect this 
	  tangent line with the x-axis and call the point as x1. x1 is the 
	  next approximation of the root. We iterate this method until we 
	  converge to the root of the function (f(x) = 0). 
 	     - Arely & Berenise Miramontes	   


3. Homework 3:

   	- Refer to the folder: Homework3
	- To run Trapezoidal rule, open plotErr.m and run the script, 
	  making sure trapRule.m is 
	  in the same folder.
	- To run Gauss Quadrature, open gq_test.m, making sure lglnodes.m 
	  is in the same folder.
	- We use the following functions, f(x) = exp(cos(pi*x)) and 
	  f(x) = exp(cos(pi^2*x)) to approximate the integral of the function 
	  from [a,b] using two methods: Trapexoidal
	  Rule and Gauss Quadrature.
	     - Arely & Berenise Miramontes


4. Homework 4: 

   	- We ran the program homework4.f90 to create output text files
	  & we added the u mapping to plot a sector at an annulus.
	- We are making the error plot.
	- We still have to approximate the integral and compare it to
	  the integral using Trapezoidal Rule and then we have to plot
	  the error.
	     - Arely & Berenise Miramontes


5. Homework 5: 

        - In this report we discuss a problem of immense societal importance; 
	  the simulation of angry birds. We will experiment with the 
	  mathematical model.
	- We used the Second order Runge-Kutta method to implement a numerical
	  integration. 
	- This model is mostly a representation of two species in which one is
	  prey and the other a predator. 
	     - Arely & Berenise Miramontes
