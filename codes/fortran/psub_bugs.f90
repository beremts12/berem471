!
! Be careful what you stick into subroutines! 
!
program psub_bugs
  implicit none
  real(kind = 8) :: x,y
  real :: s
  integer :: k
  x = 3.d0 ; s = 3.0 ; k = 3
  call mysub(x,y)
  write(*,*) "x = ",x, "and y = ",y
  call mysub(s,y)
  write(*,*) "x = ",s, "and y = ",y
  call mysub(k,y)
  write(*,*) "x = ",k, "and y = ",y
end program psub_bugs

subroutine mysub(x,y)
  implicit none
  real(kind = 8), intent(in)  :: x
  real(kind = 8), intent(out) :: y
  y =  x*x
end subroutine mysub
