module xycoord
  real(kind = 8), parameter :: pi = acos(-1.d0)
  save
contains
  
  real(kind=8) function x_coord(r,s)
    implicit none
    real(kind=8) r,s
   ! x_coord = (6.5d0+r+5.d0*s)*cos(4.d0*s)
     x_coord = (2.d0+r+0.2*sin(5.d0*pi*s))*cos(0.5d0*pi*s)
   ! x_coord = r
   ! x_coord = r+0.1d0*s

 end function x_coord

  real(kind=8) function y_coord(r,s)
    implicit none
    real(kind=8) r,s
   ! y_coord = (6.5d0+r+5.d0*s)*sin(4.d0*s)
     y_coord = (2.d0+r+0.2*sin(5.d0*pi*s))*sin(0.5d0*pi*s)
   ! y_coord = s + s*r**2 
   ! y_coord = s

end function y_coord

  real(kind=8) function u_coord(x_coord,y_coord)
    implicit none
    real(kind=8) x_coord,y_coord
   ! u_coord = x_coord**2+y_coord**2
     u_coord = exp(x_coord+y_coord)
   ! u_coord = sin(x_coord)*cos(y_coord)

 end function u_coord
    
end module xycoord
