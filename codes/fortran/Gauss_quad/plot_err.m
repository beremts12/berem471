% Simple program for generating a plot
clear
clc

A = importdata('exact_curve.plt')
AMatrix = A.data

B = importdata('points.plt')
BMatrix = B.data

set(gca,'fontsize',18)
loglog(AMatrix(:,1),AMatrix(:,2),'o-',BMatrix(:,1),BMatrix(:,2),'rx--','linewidth',2)
xlabel('number of gridpoints')
ylabel('Maximum error')
legend('Computed error','n^{-2}')

print -depsc2 error_v1
print -dpng error_v1
