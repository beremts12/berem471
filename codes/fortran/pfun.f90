!
! Simple program that illustrates the use of a function.
!

program pfun
  implicit none
  real(kind = 8) :: x,y
  real(kind = 8), external  :: myfun
  x = 3.d0
  y = myfun(x)
  write(*,*) "x = ",x, "and y = ",y
end program pfun

real(kind = 8) function myfun(x)
  real(kind = 8), intent(in) :: x
  myfun = x*x
end function myfun
