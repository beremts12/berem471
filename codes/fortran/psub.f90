!
! Simple example of a subroutine
!
program psub
  implicit none
  real(kind = 8) :: x,y
  x = 3.d0
  call mysub(x,y)
  write(*,*) "x = ",x, "and y = ",y
end program psub

subroutine mysub(x,y)
  implicit none
  real(kind = 8), intent(in)  :: x
  real(kind = 8), intent(out) :: y
  y =  x*x
end subroutine mysub
