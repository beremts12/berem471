!
! A very simple exmple for math 471
!
program ex1
  implicit none
  real(kind=8) :: a,b,c
  
  a = 1.d0
  b = exp(a)
  c = a+b
  print *, "The output is = ", c
end program ex1
