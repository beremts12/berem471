program safe_sendrecv
  use mpi
  implicit none
  integer :: ierr, nprocs, myid
  integer, parameter :: nmax = 10000
  integer :: size,tag1,tag2,from,to
  real(kind =8) :: m_out(nmax),m_in(nmax)
  integer :: status(MPI_STATUS_SIZE)
  
  call mpi_init(ierr)
  call mpi_comm_size(MPI_COMM_WORLD, nprocs, ierr)
  call mpi_comm_rank(MPI_COMM_WORLD, myid, ierr)
  do size = 1,nmax,1
     if(myid == 0) then
        from = 1
        to   = 1
     elseif(myid == 1) then
        from = 0
        to   = 0
     end if
     call MPI_Sendrecv(m_out,size,MPI_DOUBLE_PRECISION,to,123,&
          m_in,size,MPI_DOUBLE_PRECISION,from,123,MPI_COMM_WORLD,status,ierr)
     if(myid == 1) then
        write(*,*) 'Proc 1 done, size is: ', size
     end if
  end do
  call mpi_finalize(ierr)
end program safe_sendrecv
