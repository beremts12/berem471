program lat_bw
  use mpi
  implicit none
  integer, parameter :: nmax = 600000
  integer :: ierr, nprocs, myid,to,size_pow,size,reps,reps_max
  real(kind = 8) :: x_from(nmax),x_to(nmax)
  real(kind = 8) :: T1,T2  
  integer :: status(MPI_STATUS_SIZE)    

  call mpi_init(ierr)
  call mpi_comm_size(MPI_COMM_WORLD, nprocs, ierr)
  call mpi_comm_rank(MPI_COMM_WORLD, myid, ierr)

  x_from = dble(myid)
  x_to = 0.d0
  
  do size_pow = 1,19
     size = 2**size_pow
     reps_max=2**(19-size_pow)
     if(myid == 0) then
        T1=MPI_Wtime() 
        do reps = 1,reps_max
           ! Processor one sends to everyone
           do to = 1,nprocs-1 
              call MPI_Send(x_from,size,MPI_DOUBLE_PRECISION,to,reps,MPI_COMM_WORLD,ierr)     
              ! Synopsis
              ! int MPI_Send(
              ! const void *buf,       :: initial address of send buffer (choice)
              ! int count,             :: number of elements in send buffer (nonnegative integer)
              ! MPI_Datatype datatype, :: datatype of each send buffer element (handle)
              ! int dest,              :: rank of destination (integer) 
              ! int tag,               :: message tag (integer)
              ! MPI_Comm comm)         :: communicator (handle)     
           end do
        end do
     else
        do reps=1,reps_max
           call MPI_Recv(x_to,size,MPI_DOUBLE_PRECISION,0,reps,MPI_COMM_WORLD, &
                status,ierr)
        end do
     endif
     call MPI_Barrier(MPI_COMM_WORLD,ierr)
     if(myid == 0) then
        T2=MPI_Wtime() 
        write(*,*) size*8,(t2-t1)/dble(reps_max*(nprocs-1)) 
     end if
  end do
  call mpi_finalize(ierr)
end program lat_bw
