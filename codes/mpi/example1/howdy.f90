program HOWDY
  use mpi
  implicit none
  integer :: ierr, nprocs, myid
  call mpi_init(ierr)
  call mpi_comm_size(MPI_COMM_WORLD, nprocs, ierr)
  call mpi_comm_rank(MPI_COMM_WORLD, myid, ierr)
  
  write(*,*) 'Howdy, I am ', myid, ' out of ',nprocs
  call mpi_finalize(ierr)
end program HOWDY
