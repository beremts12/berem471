
clear all
close all
A=load('LAT_BW16.4355033.out');
B=load('LAT_BW32.4354608.out');

N = A(:,1);
T16 = A(:,2);
T32 = B(:,2);

T_lat     = 0.3e-6; % 0.3 micro-second
Bandwidth = 10e9;    % 10   GB/second 
TModel1 = T_lat + N/Bandwidth;

T_lat     = 0.6e-6; % 0.6 micro-second
Bandwidth = 6e9;    % 6   GB/second 
TModel2   = T_lat + N/Bandwidth;

loglog(N,T16,'r*',N,T32,'k*-',N,TModel1,'r--',N,TModel2,'k--','linewidth',2);

