      SUBROUTINE mms(u,x,y,t,m)
!     This subroutine returns derivatives up to order m of 
!     the solution (a_0+a_1*x+...)(b_0+b_1*y+...)(c_0+c_1*t+...)
!     in the array u
      INTEGER m
      DOUBLE PRECISION x,y,t
      DOUBLE PRECISION u(0:m,0:m,0:m)
      DOUBLE PRECISION MatrixWithNoName(50,50,50)

      MatrixWithNoName(1,1,1) = 1.d0
      t2 = t**2
      t3 = x+1.0D0
      t4 = y+1.0D0
      t5 = cos(t2)
      t6 = sin(t2)
      t7 = t5*2.0D0
      t12 = t2*t6*4.0D0
      t8 = t7-t12
      t9 = t*t6*1.2D1
      t10 = t*t2*t5*8.0D0
      t11 = t9+t10
      MatrixWithNoName(1,1,1) = t3*t4*t6
      MatrixWithNoName(1,1,2) = t*t3*t4*t5*2.0D0
      MatrixWithNoName(1,1,3) = t3*t4*t8
      MatrixWithNoName(1,1,4) = -t3*t4*t11
      MatrixWithNoName(1,2,1) = t3*t6
      MatrixWithNoName(1,2,2) = t*t3*t5*2.0D0
      MatrixWithNoName(1,2,3) = t3*t8
      MatrixWithNoName(1,2,4) = -t3*t11
      MatrixWithNoName(2,1,1) = t4*t6
      MatrixWithNoName(2,1,2) = t*t4*t5*2.0D0
      MatrixWithNoName(2,1,3) = t4*t8
      MatrixWithNoName(2,1,4) = -t4*t11
      MatrixWithNoName(2,2,1) = t6
      MatrixWithNoName(2,2,2) = t*t5*2.0D0
      MatrixWithNoName(2,2,3) = t8
      MatrixWithNoName(2,2,4) = -t9-t10

      u = 0.d0
      u = MatrixWithNoName(1:m+1,1:m+1,1:m+1)

      return
      end
