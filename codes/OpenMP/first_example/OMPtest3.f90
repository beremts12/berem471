program OMPtest3
  !$ use omp_lib
  IMPLICIT NONE
  integer, parameter :: n = 1000
  integer :: i
  real(kind = 8) :: sum,h,x(0:n),f(0:n)
  !$ call OMP_set_num_threads(8)
  h = 2.d0/dble(n)
  !$OMP PARALLEL DO PRIVATE(i)
  do i = 0,n
     x(i) = -1.d0+dble(i)*h
     f(i) = 2.d0*x(i)
  end do
  !$OMP END PARALLEL DO
  sum = 0.d0
  !$OMP PARALLEL DO PRIVATE(i) REDUCTION(+:SUM)
  do i = 0,n-1
     sum = sum + h*f(i)
  end do
  !$OMP END PARALLEL DO
  write(*,*) "The integral is  ", sum 
end program OMPtest3
