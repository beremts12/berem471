program OMPtest
  !$ use omp_lib
  IMPLICIT NONE
  integer :: my_id,n_threads,i
  real(kind = 8) :: sum
  !$ call OMP_set_num_threads(8)
  sum = 0.d0
  !$OMP PARALLEL PRIVATE(my_id,i,sum)
  my_id = 1
  !$ my_id = OMP_get_thread_num()  
  do i = 1,my_id
     sum = sum + 1.d0
  end do
  write(*,*) "Hello world, I am ", my_id, " my sum is ", sum 
  !$OMP END PARALLEL
end program OMPtest
