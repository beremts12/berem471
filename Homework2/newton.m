% Newton's Method to solve f(x) = 0
%
% @authors: Arely and Berenise Miramontes

function [Xs,iter]=newton(f,fp,x0,Err,imax)

for i=1 : imax
    x(i) = x0 - feval(f,x0)/feval(fp,x0);
    approxErr(i) = abs((x(i) - x0)/x0)*100;
    if abs(approxErr(i) < Err)
        display(i);
       % display(approxErr(i));
        Xs = x(i);
        break
    end
    x0 = x(i);
    iter = [1:1:i];
end


if(i == imax)
    Xs = ('No solution');
end

