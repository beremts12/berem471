close all
clear
clc
% Script for using Newtons Method for solving f(x) = 0
%
% @authors: Arely and Berenise Miramontes

f = @(x) sin(x) + cos(x).^2;
fp = @(x) cos(x) - sin(2*x);

% To first have an idea of what the roots of the this polynomial will be
% we will plot the function.
hold all
x = [-1.0:0.01:4.0]';
y = f(x);
plot(x,y)
grid on

xlabel('x');
ylabel('y');
axis([-1 4 -0.5 2])


x0 = 4;
Err = 1.e-15;
imax = 1000;

[r,iter] = newton(f,fp,x0,Err,imax)

hold all
plot(r,0,'r*', 'MarkerSize',12)
legend('sin(x) + cos^2(x)','r')

