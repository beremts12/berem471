a = 1.0;   %(a-bB) is the growth rate of the insects
b = 0.5;   
c = 0.75;  %(-c+dI) is the growth rate of the birds
d = 0.25;  

%We set the initial conditions for the:
%start & stop time, initial insect & birds population

inittime = 0.0;    
initfinal = 50.0;  
I1(1) = 5.0;      
B1(1) = 1.0;     

n = 2000;                   %# of time steps
dt = (tfinal-inittime)/n;   
T = (inittime:dt:tfinal);   

%We execute the second order of the Runge-Kutta method

for i = 1:n
    
  i1 = I1(i) + 0.5*dt*I1(i)*(a-b*B1(i));
  b1 = B1(i) + 0.5*dt*B1(i)*(-c+d*I1(i));
  I1(i+1) = I1(i) + dt*i1*(a-b*b1);
  B1(i+1) = B1(i) + dt*b1*(-c+d*i1);
end;

init = sprintf('Initial Start: Birds(0)=%g, Insects(0)=%g',B1(1),I1(1));

figure(1)
clf;
plot(I1,B1,'m');
title('Plot');
xlabel('Insects');
ylabel('Birds');
legend(init,0);
grid;

figure(2)
clf;
plot(T,I1,'r-',T,B1,'k-.');
legend('Insects','Birds');
xlabel('time');
ylabel('Birds & Insects');
title(init)